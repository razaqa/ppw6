from django import forms

class TweetForm(forms.Form):
    attrs = {'type':'text',
             'class':'form-control w-50 offset-3',
             'id':'status_form'
             }

    tweet = forms.CharField(label="How's your day going?",
                            max_length=300,
                            required=True,
                            widget=forms.TextInput(attrs=attrs))
