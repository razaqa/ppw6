from django.db import models

# Create your models here.
class Tweet(models.Model):
    tweet = models.CharField(max_length=300)
    date_fill = models.DateTimeField(auto_now_add=True)
	
    def __str__(self):
        return "{}.{}".format(self.id, self.tweet)
