from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, delete_tweet
from .models import Tweet
from .forms import TweetForm
from .apps import Story6Config

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.

class Story6UnitTest(TestCase):

    def test_if_url_exist_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
        
    def test_if_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_if_page_does_not_exist(self):
        response = Client().get('wek/')
        self.assertEqual(response.status_code, 404)
        
    def test_if_index_used_the_right_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_if_delete_status_exist(self):
        response = Client().get('/hapus/')
        self.assertEqual(response.status_code, 302)

    def test_add_status_using_func(self):
        found = resolve('/simpan/')
        self.assertEqual(found.func,simpan)

    def test_hi_how_are_you_today(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hi, How are you today?',html_response)

    def test_index_does_not_have_status(self):
        response = Client().get('/')
        self.assertContains(response, "There is no status yet.")

    def test_index_have_status(self):
        # Creating a new activity
    	new_status = Tweet.objects.create(status='tes')
    	new_status.save()
    	# Retrieving all available activity
    	response = Client().get('/')
    	self.assertContains(response,'tes')

    def test_index_add_status_valid(self):
        response = Client().post('/simpan/',{'status':'Halo semua'})
        self.assertRedirects(response,'/')
        response2 = Client().get('/')
        self.assertContains(response2,'Halo semua')
        
    def test_index_add_status_not_valid(self):
        response = Client().post('/simpan/',{})
        self.assertRedirects(response, '/')
        response2 = Client().get('/')
        self.assertContains(response2,"There is no status yet.")
        

class LandingpageFormTest(TestCase):
    def test_forms_valid(self):
        data = {'status': 'Halo semua'}
        new_status = TweetForm(data = data)
        self.assertTrue(new_status.is_valid())
        self.assertEqual(new_status.cleaned_data['status'],'Halo semua')

    def test_form_is_not_valid(self):
        form = TweetForm(data = {})
        self.assertFalse(form.is_valid())


class StatusModelsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Tweet.objects.create(tweet="a status")

    def test_if_status_created(self):
        count = Tweet.objects.all().count()
        self.assertEqual(count, 1)

    def test_if_status_exist(self):
        tweet = Tweet.objects.get(id=1)
        status_fill = tweet.tweet
        self.assertEqual(status_fill,"a status")

    def test_if_status_max_length_is_300(self):
        status = Tweet.objects.get(id=1)
        status_length = status._meta.get_field('status').max_length
        self.assertEqual(status_length,300)

    def test_if_new_is_in_database(self):
        new_status = Tweet(tweet = 'tes')
        new_status.save()
        count = Tweet.objects.all().count()
        self.assertEqual(count,2)
    	
    
class FunctionalTest(StaticLiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        options.headless = True
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(LandingPageFunctionalTest, self).setUp()

    def tearDown(self):
        cls.selenium.quit()
        super(LandingPageFunctionalTest, self).tearDown()

    def test_input_form(self):
        selenium = self.selenium

        # Opening the link we want to test
        selenium.get('http://localhost:8000/')
        time.sleep(5)
            
        # find the form element
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        status.send_keys('test 123')

        # submitting the form
        submit.send_keys(Keys.RETURN)
        time.sleep(10)





        
